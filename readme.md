# Működést leíró állapot diagram

![Működést leíró állapot diagram](stateDiagram/StateDiagram.png)

# Működést leíró automata

![Működést leíró automata](automata/EFSM_Ordering.png)

# Install:

`npm i`

# Start:

`npm run dev`

Runs on http://localhost:3000/

# Export to static HTML:

`npm run export`

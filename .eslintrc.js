module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  parser: 'babel-eslint',
  extends: ['eslint:recommended'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
      modules: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['react', 'prettier'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  rules: {
    'no-undef': ['error'],
    eqeqeq: ['warn', 'smart'],
    'no-unused-vars': 'off',
    'no-prototype-builtins': 'off',
    'id-length': ['warn', { exceptions: ['i', 'j', 't', 'Q', 'A'] }],
  },
}

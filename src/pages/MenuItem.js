import styles from "./Home.module.css";
import React from "react";

export default function MenuItem(props) {
  const {name, id, cost, ammount, onAdd, onRemove, freeBev} = props;

  return (
    <div className={styles.item}>
      <div className={styles.itemName}>{name}</div>
      <div className={styles.itemProp}>{cost}</div>
      <div className={styles.itemProp}>
        <div
          className={`${styles.circleStuff} ${styles.add}`}
          onClick={() => {
            onAdd(id)
          }}
        >
          +
        </div>
      </div>
      <div className={styles.itemProp}>{ammount}</div>
      <div className={styles.itemProp}>
        {ammount > 0 && (
          <div
            className={`${styles.circleStuff} ${styles.remove}`}
            onClick={() => {
              onRemove(id)
            }}
          >
            ×
          </div>
        )}
      </div>
    </div>
  )
}
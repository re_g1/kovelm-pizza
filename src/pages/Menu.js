import React from "react";
import styles from "./Home.module.css";
import MenuItem from "./MenuItem";
import MenuTotal from "./MenuTotal";

export const items = {
  pizzaMexicana: {name: 'Pizza mexicana', cost: 7, isBeverage: false},
  pizzaRucolaXXL: {name: 'Pizza Rucola XXL', cost: 14, isBeverage: false},
  pizzaChicken: {name: 'Pizza Chicken', cost: 8.5, isBeverage: false},
  coke: {name: 'Coke', cost: 3, isBeverage: true},
  beer: {name: 'Beer', cost: 2, isBeverage: true},
};
export const minTotal = 10;
export const minForFreeBeverage = 30;

export default function Menu(props) {
  const {cart, freeBev, setFreeBev, setCart} = props;

  const total = Object.keys(cart).reduce((acc, key) => {
    acc += items[key].cost * cart[key].amm;
    return acc
  }, 0);

  const totalWithOutBev = Object.keys(cart).reduce((acc, key) => {
    if (!items[key].isBeverage) {
      acc += items[key].cost * cart[key].amm;
    }
    return acc
  }, 0);

  const handleItemAdd = (id) => {
    if (
      totalWithOutBev >= minForFreeBeverage &&
      items[id].isBeverage &&
      !freeBev
    ) {
      setFreeBev(items[id]);
      return
    }
    if (!cart[id]) {
      setCart({
        ...cart,
        [id]: {amm: 1},
      })
    } else {
      setCart({
        ...cart,
        [id]: {amm: cart[id].amm + 1},
      })
    }
  };

  const handleItemRemove = (id) => {
    if (!cart[id] || cart[id].amm - 1 < 0) {
      return
    }

    setCart({
      ...cart,
      [id]: {amm: cart[id].amm = 0},
    });
  };

  return (
    <div>
      <div className={styles.itemContainer}>
        {Object.keys(items).map((key) => {
          const item = items[key];
          return (
            <MenuItem
              id={key}
              freeBev={freeBev}
              ammount={cart[key] ? cart[key].amm : 0}
              onRemove={(id) => handleItemRemove(id)}
              onAdd={(id) => handleItemAdd(id)}
              key={key}
              {...item}
            />
          )
        })}
      </div>
      <MenuTotal total={total} totalWithOutBev={totalWithOutBev} freeBev={freeBev}/>
    </div>
  )
}
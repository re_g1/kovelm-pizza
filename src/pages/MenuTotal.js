import {minForFreeBeverage, minTotal} from "./Menu";
import styles from "./Home.module.css";
import React from "react";

export default function MenuTotal(props) {
  const {total, totalWithOutBev, freeBev} = props;
  const extras = [];

  if (total < minTotal) {
    extras.push(
      <div className={styles.orderMore}>
        You should order more for at least {minTotal - total} euro(s)
      </div>
    )
  }

  if (totalWithOutBev >= minForFreeBeverage) {
    extras.push(
      <div className={styles.beverage}>
        {'You can choose a free beverage'}
        {freeBev ? (
          <>
            {':'}
            <span className={styles.freeBevName}>{` ${freeBev.name}`}</span>
          </>
        ) : (
          '!'
        )}
      </div>
    )
  }

  return (
    <div>
      <div className={styles.total}>Total price: {total}</div>
      {extras}
    </div>
  )
}
import React, {useState} from 'react'
import styles from './Home.module.css'
import Menu from "./Menu";


export default function Home() {
  const [cart, setCart] = useState({});
  const [freeBev, setFreeBev] = useState(null);

  const handleCancellation = () => {
    setCart({});
  };

  const handlePay = () => {
    alert('Successful order!');
    setCart({});
  };

  return (
    <div className={styles.container}>
      <div className={styles.title}>Shopping</div>
      <Menu cart={cart} freeBev={freeBev} setFreeBev={setFreeBev} setCart={setCart}/>

      <div className={styles.buttonContainer}>
        <div
          className={styles.button}
          onClick={() => handleCancellation()}
        >
          Cancel
        </div>
        <div
          className={styles.button}
          onClick={() => handlePay()}
        >
          Pay
        </div>
      </div>
    </div>
  )
}
